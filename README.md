# Hadoop Project: Weather Data

## Promoting Raw Layer + NiFi

d9lcwphd0e2

pre steps:

ssh to host nifi host(s) 

* qa: q9lcwphd1e1.corp.ad.ctc
* prod: 
    * p9cpwphd2m3.corp.ad.ctc
    * p9cpwphd3m3.corp.ad.ctc
    * p9cpwphd4m3.corp.ad.ctc

```
sudo su - svc.hdetl1prd

cd to deployment folder
git clone ssh://git@bitbucket.corp.ad.ctc:22/bdhi/weather.git
cd weather
```

Promote to QA:
```
git checkout feature/weather_v1

rm -rf /apps/qat/raw/weather/
mkdir /apps/qat/raw/weather
cp -R metpx-sarracenia/ /apps/qat/raw/weather/
chmod 777 -R /apps/qat/raw/weather
```

Promoto to PROD: (needs to be done on all 3 nifi hosts)
```
git checkout release/weather_v1

mkdir /apps/prod/raw/weather
cp -R metpx-sarracenia/ /apps/prod/raw/weather/
chmod 777 -R /apps/prod/raw/weather
```

1. Go into NiFi Instance:
* QA: https://q9lcwphd1e1:8080/nifi/
* PROD: https://p9cpwphd2m3.corp.ad.ctc:9091/nifi/

2. Move into the processor group below:

    NiFi Flow  » Ingestion  » Batch Ingestion - RDBMS Sources & Others

3. Import the NiFi template to the UI that is in the git repo that was downloaded previously. You can find the Nifi template XML in: 

    `/weather/nifi/weather_nifi_20171112.xml` Place template on Canvas

4. Connect/Create a Connection **Weather Processor** Group with **Batch Ingestion Framework - RDBMS**. 
From Output **Weather Success** to **Incoming Data - RDBMS**

5. In order for the next step to be successful, sarra metpx needs to be installed. 

	Pre-req, install sarra: (refer to JIRA - https://jira.corp.ad.ctc/browse/DLP-424) 
	```
	sudo wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
	sudo rpm -ivh epel-release-latest-7.noarch.rpm
	sudo yum install python34-devel
	sudo yum install python34-setuptools
	sudo pip3 install paramiko==1.16.0
	sudo pip3 install metpx_sarracenia
	```

	QAT
	```
	sr_subscribe -n /apps/qat/raw/weather/metpx-sarracenia/samples/config/ctc/cap.conf stop
	sr_subscribe -n /apps/qat/raw/weather/metpx-sarracenia/samples/config/ctc/cap.conf start
	```

	PROD
	```
	sr_subscribe -n /apps/prod/raw/weather/metpx-sarracenia/samples/config/ctc/cap.conf stop
	sr_subscribe -n /apps/prod/raw/weather/metpx-sarracenia/samples/config/ctc/cap.conf start
	```


6. Go into NiFi UI, and go to:
    NiFi Flow  » Ingestion  » Batch Ingestion & Others  » Weather  » Weather - Alerts

    Right Click on "Alerts - Trigger" processor -> Configure
    Properties -> Password - > Update to "anonymous"

7. For CityPageWeather and Observations, Scheduling needs to be: `0 50 13 * * ?`

8. Create logs directory in all 3 NiFi nodes: (With svc.weatherp)
	p9cpwphd2m3.corp.ad.ctc
    p9cpwphd3m3.corp.ad.ctc
    p9cpwphd4m3.corp.ad.ctc 
	
    ```	
        mkdir -p -m 777 /apps/prod/raw/corp_envcanada/logs
    ```

9. Please ensure Execution (Under Schedulng tab) is set to "Primary Node" 

10. Start all processors in:
    
    NiFi Flow  » Ingestion  » Batch RDBMS Ingestion & Others  » Weather



### Promoting Datavault and Businessvault layers to QAT for below tables


```
	alerts
	citypageweather
	observationsswob
```


Getting Started:

N/A Built With (list technologies)

Hadoop tech stacks

Deployment Notes

Release - Date: 2017-11-03

Summary:
Description: This RAW-BV database deployement is brand new code to get the data into data lake.

As part of this process one raw layer new database and associated tables needs to be created in hive and these are:


```
datavault_staging_unencrypted.dv_stag_alerts
datavault_unencrypted.hub_weather_station
datavault_unencrypted.sat_weather_alerts
datavault_staging_unencrypted.dv_stag_citypage_almanac
datavault_unencrypted.hub_weather_city
datavault_unencrypted.sat_link_weather_almanac
datavault_unencrypted.sat_weather_date
datavault_staging_unencrypted.dv_stag_citypage_current_condition
datavault_unencrypted.hub_weather_date
datavault_unencrypted.link_weather_date_city
datavault_unencrypted.sat_link_weather_current_condition
datavault_unencrypted.sat_weather_city
datavault_staging_unencrypted.dv_stag_citypage_forecast
datavault_unencrypted.sat_link_weather_forecast
datavault_staging_unencrypted.dv_stag_citypage_hourly_forecast
datavault_unencrypted.sat_link_weather_hourly_forecast
datavault_staging_unencrypted.dv_stag_citypage_regionalnominals
datavault_unencrypted.sat_link_weather_forecast_regional_nominals
datavault_unencrypted.sat_link_weather_warning
datavault_staging_unencrypted.dv_stag_citypage_yesterday_condition
datavault_unencrypted.sat_link_weather_yesterday_condition
datavault_staging_unencrypted.citypage_almanac
datavault_staging_unencrypted.citypage_current_condition
datavault_staging_unencrypted.citypage_forecast
datavault_staging_unencrypted.citypage_regionalnominals
datavault_staging_unencrypted.citypage_hourly_forecast
datavault_staging_unencrypted.citypage_yesterday_condition
datavault_staging_unencrypted.dv_stag_observationsswob
datavault_unencrypted.sat_weather_station_details
datavault_unencrypted.sat_weather_station_observation
datavault_unencrypted.ref_ctc_corp_envcanada_weather_icons
businessvault_unencrypted.base_corp_envcanada_weather_yesterday_condition
businessvault_unencrypted.base_corp_envcanada_weather_station_condition
businessvault_unencrypted.base_corp_envcanada_weather_station_details
businessvault_unencrypted.base_corp_envcanada_weather_hourly_forecast
businessvault_unencrypted.base_corp_envcanada_weather_forecast
businessvault_unencrypted.base_corp_envcanada_weather_current_condition
businessvault_unencrypted.base_corp_envcanada_weather_city
businessvault_unencrypted.base_corp_envcanada_weather_almanac
businessvault_unencrypted.base_corp_envcanada_weather_alerts
```



Owner Name: SLA requirements for delivery time (on daily, weekly, or monthly basis): daily

Data Classification of outputs (encrypted/unencrypted): unencrypted

Inputs required for the job/s: none

Source data: Environment Canada

Is this currently being ingested? (Y/N): N

Frequency of ingest: daily

Volume of data expected (ie GB/row size): < 1,000,000 records

Job Requirements:

Any external libraries or requirements that are not met by the base installation of the stack: none

Service Account details: svc.weatherp

Ranger Policy link: ```https://confluence.corp.ad.ctc/display/DS/Ranger+policy```

Needs to run as user: * Needs to run as user: Respective Hadoop QAT Service Account

Implementation Details:

Dependency List:
	a. PROD hadoop edge node server should be up and running.
	b. below common folders should already be there. /apps/prod/

Steps for Pre-Implementation:
	1.Please Check if below databases have been already exist in QAT/PROD. If not, please create them properly. - datavault_staging_unencrypted - datavault_unencrypted - businessvault_unencrypted
	2.Please Make sure the NIFI implementation in branch 'Reference_tables' in REPO ims has been completed. no NIFI implementation required for this branch.

Dependency List: a. PROD hadoop endge node server and NiFi should be up and runing.
	a. below common folders should already be there. /apps/prod/
	b. datavault_staging_unencrypted ,datavault_unencrypted and businessvault_unencrypted databases should already exist.

	
	
## Steps to Implemetation:
```
	Run GIT clone as below ( Please replace xxxxxxx below with your lan-id ) in Edge node home directory
	https://xxxxxxxxxx@bitbucket.corp.ad.ctc/scm/bdhi/weather.git

	QAT: git clone -b feature/weather_v1

	Prod: git clone -b release/weather_v1
```

1. Run as admin:
```
	For qat:  hmod 777 /apps/qat/datavault
	For prod: chmod 777 /apps/prod/datavault
```

2. Run as admin:
```
	For qat:  chmod 777 /apps/qat/businessvault
	For prod: chmod 777 /apps/prod/businessvault
```

3. Run below script (found in home directory) on QAT/Prod to create folder structures 
   (svc.weatherp needs to have write permision on /apps/qat/datavault and /apps/qat/businessvault).
   (svc.weatherp needs to have write permision on /apps/prod/datavault and /apps/prod/businessvault).
```	
	Please connect with service account , then execute these below commands:
		
	Service Account: svc.weatherp
	
	For QAT: ./create_dir_and_copy_scripts.sh qat 
		
	For Production: ./create_dir_and_copy_scripts.sh prod
	
```

4. Run below script (from in home directory), which creates proper database (corp_envcanada) and tables on QAT/PROD. ( Hadoop Admin task )
```
	For QAT: ./create_hive_table.sh qat 
		
	For Production: ./create_hive_table.sh prod
```

5. Batch control inserts in mysql
```
	cd mysql
```
i) For QAT: follow below steps in Prod My SQL DB instance.
```
	a. Type command "mysql -h q9lcwphwsql04.labcorp.ad.ctc -u SVC.BATCHCTRL  -p"

	b. Enter the password to go to mysql prompt. (Please contact if you require the PW)
		
	c. At mysql prompt type command "source sql_batch_control_entry_qat.sql;"
```
ii) For PROD: follow below steps in Prod My SQL DB instance.
```
	a. Type command "mysql -h P9CPWPHWSQL02.corp.ad.ctc -u SVC.BATCHCTRL  -p"

	b. Enter the password to go to mysql prompt. (Please contact if you require the PW)
		
	c. At mysql prompt type command "source sql_batch_control_entry_prod.sql;"
```



6. Run the following command to copy ref_ctc_corp_envcanada_weather_icons: (needs to be run with ```admin account```)
```
	hdfs dfs -put -f weather_icons /data/unencrypted/datavault/ref_ctc_corp_envcanada_weather_icons
```
***Note: this is a reference file and will be uploaded once.****	


7. Run the following command to copy ref_ctc_corp_envcanada_city_nm_mapping: (needs to be run with ```admin account```)
```
	hdfs dfs -put -f city_name_mapping /data/unencrypted/datavault/ref_ctc_corp_envcanada_city_nm_mapping
```
***Note: this is a reference file and will be uploaded once.****


8. Run the following command to copy ref_ctc_corp_envcanada_store_nm_mapping: (needs to be run with ```admin account```)
```
	hdfs dfs -put -f store_city_mapping /data/unencrypted/datavault/ref_ctc_corp_envcanada_store_nm_mapping
```
***Note: this is a reference file and will be uploaded once.****

9. Run the following command to open netezza password file and then add the PROD password for SVC.BIETL.DLSTAGP in the file:  (needs to be run with ```svc.weatherp```)
```
	vi netezza_prd_stag.pass (please add the password and save it, also kindly ensure that there are no extra characters inside the file)
```
 
10. Run the following command to copy netezza password file to the home directory of svc.weatherp:  (needs to be run with ```svc.weatherp```)

```
    For QA: hdfs dfs -put -f netezza_qat_stag.pass .
	For PROD: hdfs dfs -put -f netezza_prd_stag.pass .
```
 ***This will be used by ESP to SQOOP data to EDW.***
 
11. Run as admin:
```
	For qat:  chmod 775 /apps/qat/datavault
	For prod: chmod 775 /apps/prod/datavault
```

12. Run as admin:
```
	For qat:  chmod 775 /apps/qat/businessvault
	For prod: chmod 775 /apps/prod/businessvault
```


++++++++++++++++++++++++++++++++++++++

Datavault loading process: For QA.

++++++++++++++++++++++++++++++++++++++

Make sure the other Branch release/Reference table in repository corp_envcanada has been implemented and initial run has been done. 
Run below script step by step to populate the Datavault tables in QAT Edge node server.

	======= CITYPAGE RAW TO STAG========
		/apps/qat/datavault/corp_envcanada/citypageweather/script/load_raw_citypageweather_to_datavault.sh 20171020 130000
	
	====== BATCH CONTROL EXECUTION ENTRIES  =======
	/apps/qat/datavault/corp_envcanada/citypageweather/script/dv_batch_control_citypageweather.ksh 20171020 130000

	======= CITYPAGE STAG TO DATAVAULT========
		/apps/dev/datavault/corp_envcanada/citypage_almanac/script/load_raw_citypage_almanac_to_datavault.sh 20171020 130000
		/apps/dev/datavault/corp_envcanada/citypage_current_condition/script/load_raw_citypage_current_condition_to_datavault.sh 20171020 130000
		/apps/dev/datavault/corp_envcanada/citypage_forecast/script/load_raw_citypage_forecast_to_datavault.sh 20171020 130000
		/apps/dev/datavault/corp_envcanada/citypage_hourly_forecast/script/load_raw_citypage_hourly_forecast_to_datavault.sh 20171020 130000
		/apps/dev/datavault/corp_envcanada/citypage_regionalnominals/script/load_raw_citypage_regionalnominals_to_datavault.sh 20171020 130000
		/apps/dev/datavault/corp_envcanada/citypage_yesterday_condition/script/load_raw_citypage_yesterday_condition_to_datavault.sh 20171020 130000

	======= OBSERVATION DATAVAULT========
		/apps/qat/comm_exe/load_datavault_fgl.sh corp_envcanada corp_envcanada_unencrypted_raw.observationsswob datavault_staging_unencrypted.dv_stag_observationsswob truncate_and_load_dv_stag_observationsswob.hql.config 20171020 130000
		/apps/qat/comm_exe/load_datavault_fgl.sh corp_envcanada corp_envcanada_unencrypted_raw.observationsswob datavault_unencrypted.hub_weather_station load_hub_weather_station_from_observationsswob.hql.config 20171020 130000
		/apps/qat/comm_exe/load_datavault.sh corp_envcanada corp_envcanada_unencrypted_raw.observationsswob datavault_unencrypted.sat_weather_station_details load_sat_weather_station_details_from_observationsswob.hql.config 20171020 130000
		/apps/qat/comm_exe/load_datavault_fgl.sh corp_envcanada corp_envcanada_unencrypted_raw.observationsswob datavault_unencrypted.sat_weather_station_observation load_sat_weather_station_observation_from_observationsswob.hql.config 20171020 130000

	======= ALERTS DATAVAULT========
		/apps/qat/comm_exe/load_datavault_fgl.sh corp_envcanada corp_envcanada_unencrypted_raw.alerts datavault_staging_unencrypted.dv_stag_alerts truncate_and_load_dv_stag_alerts.hql.config 20171020 130000
		/apps/qat/comm_exe/load_datavault_fgl.sh corp_envcanada corp_envcanada_unencrypted_raw.alerts datavault_unencrypted.hub_weather_station load_hub_weather_station_from_alerts.hql.config 20171020 130000
		/apps/qat/comm_exe/load_datavault_fgl.sh corp_envcanada corp_envcanada_unencrypted_raw.alerts datavault_unencrypted.sat_weather_alerts load_sat_weather_alerts_from_alerts.hql.config 20171020 130000

	======= CITYPAGE BUSINESSVAULT========
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_city load_base_corp_envcanada_weather_city.hql.config 20171020 130000
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_almanac load_base_corp_envcanada_weather_almanac.hql.config 20171020 130000
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_current_condition load_base_corp_envcanada_weather_current_condition.hql.config 20171020 130000
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_forecast load_base_corp_envcanada_weather_forecast.hql.config 20171020 130000
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_hourly_forecast load_base_corp_envcanada_weather_hourly_forecast.hql.config 20171020 130000
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_yesterday_condition load_base_corp_envcanada_weather_yesterday_condition.hql.config 20171020 130000

	======= WEATHER_HISTORY BUSINESSVAULT========
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted weather_history load_weather_history_fx.hql.config 20171020 130000
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted weather_history load_weather_history_obs.hql.config 20171020 130000

	======= OBSERVATION BUSINESSVAULT========
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_station_condition load_base_corp_envcanada_weather_station_condition.hql.config 20171020 130000
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_station_details load_base_corp_envcanada_weather_station_details.hql.config 20171020 130000

	======= ALERTS BUSINESSVAULT========
		/apps/qat/comm_exe/load_businessvault.sh corp_envcanada businessvault_unencrypted base_corp_envcanada_weather_alerts load_base_corp_envcanada_weather_alerts.hql.config 20171020 130000

	======= WEATHER SQOOP TO EDW========
	/apps/qat/comm_exe/sqoop_export_rdbms.ksh businessvault corp_envcanada/weather_history sqoop_weather_history_to_edw.config 20171027 140000



## Rollback Steps
		
Steps to Rollback:

1.Go to sql directory from root as below

	cd mysql

i) If the QAT space was used to promote, then follow below steps in QA My SQL DB instance to rollback.
```
	a. Type command "mysql -h q9lcwphwsql04.labcorp.ad.ctc -u SVC.BATCHCTRL  -p"

	b. Enter the password to go to mysql prompt. (Please contact if you require the PW)

	c. At mysql prompt type command "source sql_batch_control_delete_qat.sql;"
```
ii) If the Prod space was used to promote, then follow below steps in Prod My SQL DB instance to rollback.
```
	a. Type command "mysql -h P9CPWPHWSQL02.corp.ad.ctc -u SVC.BATCHCTRL  -p"

	b. Enter the password to go to mysql prompt. (Please contact if you require the PW).

	c. At mysql prompt type command "source sql_batch_control_delete_prod.sql;"
```

2. Run as admin:
```
	For qat:  hmod 777 /apps/qat/datavault
	For prod: chmod 777 /apps/prod/datavault
```

3. Run as admin:
```
	For qat:  chmod 777 /apps/qat/businessvault
	For prod: chmod 777 /apps/prod/businessvault
```

4.Run below script (from in home directory), which drop proper database (corp_envcanada) and tables on QAT/PRD. ( Hadoop Admin task )
```
	For QAT: ./rollback_hive.sh qat 

	For Production: ./rollback_hive.sh prod
```

5.Run below script to remove the directories from Edge node. ( Hadoop Admin task )
```
	For QAT: ./rollback_dir.sh qat 

	For Production: ./rollback_dir.sh prod
```

6.Remove .pass file from svc.weatherp users HDFS home directory ( (needs to be run with ```svc.weatherp```) )
```
	hdfs dfs -rm /user/svc.weatherp/netezza_prd_stag.pass
```
7.Remove git clone from your home directory.
```
	cd ~ rm -R weather
```

8. Run as admin:
```
	For qat:  chmod 775 /apps/qat/datavault
	For prod: chmod 775 /apps/prod/datavault
```

9. Run as admin:
```
	For qat:  chmod 775 /apps/qat/businessvault
	For prod: chmod 775 /apps/prod/businessvault
```
